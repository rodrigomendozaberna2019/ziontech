import React from 'react'
import GoogleMapReact from 'google-map-react'

const defaultProps = {
    center: {
        lat: 59.95,
        lng: 30.33,
    },
    zoom: 11,
}

const AnyReactComponent = ({ text }) => <div>{text}</div>

const GoogleMap = () => (
    <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
            // bootstrapURLKeys={{ key: 'AlzaSyBRKoTpCZsUXfcXyxHxoK-PpXMHYwfqs8o' }}
            bootstrapURLKeys={{ key: 'AIzaSyCKx4M3ToUPDWH0qVsm_542atQwlBKW7J8' }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
        >
            <AnyReactComponent
                lat={19.4672029}
                lng={-99.1830049}
                text={'Mexico'}
            />
        </GoogleMapReact>
    </div>
)

export default GoogleMap
